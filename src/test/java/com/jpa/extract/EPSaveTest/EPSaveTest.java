package com.jpa.extract.EPSaveTest;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.jpa.extract.common.constant.ApiConstant;
import com.jpa.extract.common.util.ConvertUtil;
import com.jpa.extract.common.util.RestTemplateUtil;
import com.jpa.extract.domain.Product;
import com.jpa.extract.domain.ProductMeta;
import com.jpa.extract.repository.ProductMetaRepository;
import com.jpa.extract.repository.ProductRepository;

/**
 * =====================================================
 * @author | jaeyoung
 * @mkdate | 2019. 8. 25.
 * @file   | EPSaveTest.java
 * @desc   | 네이버 EP 데이터 삭제&조회&저장
 * =====================================================
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class EPSaveTest {
	private static final Logger logger = LoggerFactory.getLogger(EPSaveTest.class);
	
	@Value("${jy.naver.client.key}")
	private String clientKey;
	@Value("${jy.naver.server.key}")
	private String serverKey;
	
	@Autowired
	private ProductMetaRepository prdMetaRepo;
	
	@Autowired
	private ProductRepository prdRepo;
	
	/**
	 * ep data 테스트 저장용
	 * meta 데이터와 item 정보를 따로 분리하여 받아옴! >> map.get("meta") | map.get("itemList")
	 * * 공통(컨버팅)으로 사용할 수 있도록 고민해보자
	 */
	@SuppressWarnings("unchecked")
	@Before
	public void before() {
		// 이전 데이터 삭제!!
		prdMetaRepo.deleteAll();
		prdRepo.deleteAll();
		
		Map<String, Object> reqMap = new HashMap<String, Object>();
		
        reqMap.put(ApiConstant.URL, "https://openapi.naver.com/v1/search/shop.json?query=hand"); 
        reqMap.put(ApiConstant.HTTP_METHOD, HttpMethod.GET);
        reqMap.put(ApiConstant.NAVER_CLI_KEY, clientKey);
        reqMap.put(ApiConstant.NAVER_SEV_KEY, serverKey);
        reqMap.put(ApiConstant.MEDIA_TYPE, new MediaType(MediaType.APPLICATION_JSON, Charset.forName("UTF-8")));

        ResponseEntity<?> resEntity = RestTemplateUtil.openApiRequest(reqMap);
        
        // POJO 객체로 받자.... > 맵으로 들오넹 >> 파싱 (key & value)
        Map<String, Object> resMap = (Map<String, Object>) resEntity.getBody();
        
        Map<String, Object> resultMap = ConvertUtil.convertEPtoMap(resMap);
        
        ProductMeta prdMeta = (ProductMeta) resultMap.get("meta");
        List<Product> itemList = (List<Product>) resultMap.get("itemList");
        
        prdMetaRepo.save(prdMeta); 
        itemList.forEach(item -> prdRepo.save(item));
	}
	
	@Test
	public void test() {
		logger.info("========================= data check start =========================");
		
		List<ProductMeta> prdMetaList = prdMetaRepo.findAll();
		prdMetaList.forEach(prdMeta -> logger.info(prdMeta.toString()));
		
		List<Product> prdList = prdRepo.findAll();
		prdList.forEach(prd -> logger.info(prd.toString()));
	}
}

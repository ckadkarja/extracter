package com.jpa.extract.api.ep.service.impl;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.jpa.extract.api.ep.service.EpService;
import com.jpa.extract.common.constant.ApiConstant;
import com.jpa.extract.common.util.ConvertUtil;
import com.jpa.extract.common.util.RestTemplateUtil;
import com.jpa.extract.domain.Product;
import com.jpa.extract.domain.ProductMeta;
import com.jpa.extract.model.SearchVO;
import com.jpa.extract.repository.ProductMetaRepository;
import com.jpa.extract.repository.ProductRepository;

@Service("epService")
public class EpServiceImpl implements EpService {

	@Value("${jy.naver.client.key}")
	private String clientKey;
	@Value("${jy.naver.server.key}")
	private String serverKey;
	
	@Autowired
	private ProductMetaRepository prdMetaRepo;
	
	@Autowired
	private ProductRepository prdRepo;
	
	@Override
	public void getEpData(SearchVO searchVO) throws Exception {
		Map<String, Object> reqMap = new HashMap<String, Object>();
		
        reqMap.put(ApiConstant.URL, "https://openapi.naver.com/v1/search/shop.json?query=" + ConvertUtil.convertQueryStr(searchVO)); 
        reqMap.put(ApiConstant.HTTP_METHOD, HttpMethod.GET);
        reqMap.put(ApiConstant.NAVER_CLI_KEY, clientKey);
        reqMap.put(ApiConstant.NAVER_SEV_KEY, serverKey);
        reqMap.put(ApiConstant.MEDIA_TYPE, new MediaType(MediaType.APPLICATION_JSON, Charset.forName("UTF-8")));

        ResponseEntity<?> resEntity = RestTemplateUtil.openApiRequest(reqMap);
        
        // POJO ��ü�� ����.... > ������ ����� >> �Ľ� (key & value)
        Map<String, Object> resMap = (Map<String, Object>) resEntity.getBody();
        
        Map<String, Object> resultMap = ConvertUtil.convertEPtoMap(resMap);
        
        ProductMeta prdMeta = (ProductMeta) resultMap.get("meta");
        List<Product> itemList = (List<Product>) resultMap.get("itemList");
        
        prdMetaRepo.save(prdMeta); 
        itemList.forEach(item -> prdRepo.save(item));
	}
	
}

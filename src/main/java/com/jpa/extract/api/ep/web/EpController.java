package com.jpa.extract.api.ep.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.jpa.extract.api.ep.service.EpService;
import com.jpa.extract.model.SearchVO;

/**
 * =====================================================
 * @author | jaeyoung
 * @mkdate | 2019. 8. 25.
 * @file   | EPSaveTest.java
 * @desc   | 네이버 EP 데이터 삭제&조회&저장
 * =====================================================
 */
@RestController
public class EpController {
	private static final Logger logger = LoggerFactory.getLogger(EpController.class);
	
	@Autowired
	private EpService epService;
	
	// TODO ! 트랜잭션 처리 ( url 처리로 인한 get )
	@GetMapping("/saveEpData/{searchItem}") 
	public void saveEpData(@PathVariable(value="searchItem") String searchItem) {
		SearchVO searchVO = new SearchVO();
		
		if (searchItem == null) {
			logger.info(" 검색 대상 입력! ");
		}
		
		try {
			searchVO.setSearchItem(searchItem);
			
			epService.getEpData(searchVO);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

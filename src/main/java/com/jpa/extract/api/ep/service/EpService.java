package com.jpa.extract.api.ep.service;

import com.jpa.extract.model.SearchVO;

public interface EpService {

	void getEpData(SearchVO searchVO) throws Exception;

}

package com.jpa.extract.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jpa.extract.domain.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

}

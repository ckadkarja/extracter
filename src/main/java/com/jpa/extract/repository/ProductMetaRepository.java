package com.jpa.extract.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jpa.extract.domain.ProductMeta;

public interface ProductMetaRepository extends JpaRepository<ProductMeta, Long> {

}

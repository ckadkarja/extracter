package com.jpa.extract.common.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jpa.extract.domain.Product;
import com.jpa.extract.domain.ProductMeta;
import com.jpa.extract.model.SearchVO;

/**
 * =====================================================
 * @author | jaeyoung
 * @mkdate | 2019. 8. 25.
 * @file   | ConvertUtil.java
 * @desc   | converting 유틸
 * =====================================================
 */
public class ConvertUtil {
	private static final Logger logger = LoggerFactory.getLogger(ConvertUtil.class);
	
	/**
	 * Naver EP(Shopping) 데이터 컨버팅 >> 공통적으로 현재 활용불가능
	 * @parma map : rest api return data -> getBody()
	 * @return map key : meta > ProductMeta, itemList > List<Product>
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, Object> convertEPtoMap(Map<String, Object> resMap) {
		ProductMeta meta = ProductMeta.builder()
								.display((int) resMap.get("display"))
								.lastBuildDate(String.valueOf(resMap.get("lastBuildDate")))
								.start((int) resMap.get("start"))
								.total((int) resMap.get("total"))
								.build();
		
		// itemList 추출 (jsonArray 형태)
		List<Product> itemList = new ArrayList<Product>();
		List<Map<String, Object>> list = (List<Map<String, Object>>) resMap.get("items");
		
		for (int i = 0; i < list.size(); i++) {
			Map<String, Object> tempObject = list.get(i);
			
			try {
				Product item = Product.builder()
									.title(String.valueOf(tempObject.get("title")))
									.link(String.valueOf(tempObject.get("link")))
									.productId(Long.valueOf(String.valueOf(tempObject.get("productId"))))
									.productType(Integer.parseInt(String.valueOf(tempObject.get("productType"))))
									.hprice(Long.valueOf(String.valueOf(tempObject.get("hprice"))))
									.lprice(Long.valueOf(String.valueOf(tempObject.get("lprice"))))
									.mallName(String.valueOf(tempObject.get("mallName")))
									.image(String.valueOf(tempObject.get("image")))
									.productMeta(meta)
									.build(); 
				itemList.add(item);
			} catch (Exception e) {
				e.printStackTrace(); // check
			//	logger.error("product parsing error : " + e.getMessage());
			}
		}
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		resultMap.put("meta", 		meta);
		resultMap.put("itemList", 	itemList);
		
		return resultMap;
	}
	
	public static String convertQueryStr(SearchVO searchVO) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("query" + searchVO.getSearchItem());
		sb.append("&display=" + searchVO.getDisplay());
		sb.append("&start=" + searchVO.getStart());
		sb.append("&sort=" + searchVO.getSort());
		
		return sb.toString();
	}
}

package com.jpa.extract.common.util;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.jpa.extract.common.constant.ApiConstant;
import com.jpa.extract.config.RestTemplateInstance;

/**
 * =====================================================
 * @author | jaeyoung
 * @mkdate | 2019. 8. 25.
 * @file   | RestTemplateUtils.java
 * @desc   | rest template 사용 유틸
 * =====================================================
 */
public class RestTemplateUtil {

	public static ResponseEntity<?> openApiRequest(Map<String,Object> paramMap) throws HttpClientErrorException {
        String url = (String) paramMap.get(ApiConstant.URL);
        HttpMethod httpMethod = (HttpMethod) paramMap.get(ApiConstant.HTTP_METHOD);
        HttpEntity<?> requestEntity = setHttpEntity(paramMap);
        RestTemplate restTemplate = RestTemplateInstance.getInstance().getRestTemplate();

        ResponseEntity<?> resEntity = null;
        
        resEntity = restTemplate.exchange(url, httpMethod, requestEntity, Object.class);
        
        return resEntity;
    }

    private static HttpEntity<?> setHttpEntity(Map<String, Object> paramMap){
        HttpHeaders headers= new HttpHeaders();
        headers.setAcceptCharset(Arrays.asList(Charset.forName("UTF-8")));

        if (paramMap.containsKey(ApiConstant.MEDIA_TYPE)) {
            MediaType mediaType = (MediaType) paramMap.get(ApiConstant.MEDIA_TYPE);
            headers.setContentType(mediaType);
        }
        
        String clientKey = String.valueOf(paramMap.get(ApiConstant.NAVER_CLI_KEY));
        String serverKey = String.valueOf(paramMap.get(ApiConstant.NAVER_SEV_KEY));
        
        // is Naver api? >> naver전용 헤더값이긴한데... 걍 파라미터값으로 넘기게끔 할까;
        if (!clientKey.equals("") && !serverKey.equals("")) {
        	headers.add("X-Naver-Client-Id", clientKey);
        	headers.add("X-Naver-Client-Secret", serverKey);
        }
        
        HttpEntity<?> requestEntity;
        
        // post & get transfer
        if (((HttpMethod) paramMap.get(ApiConstant.HTTP_METHOD)) != HttpMethod.GET) {
        	requestEntity = new HttpEntity<>(paramMap.get(ApiConstant.PARAM_DATA), headers);
        } else {
        	requestEntity = new HttpEntity<>(headers);
        }
        
        return requestEntity;
    }
}

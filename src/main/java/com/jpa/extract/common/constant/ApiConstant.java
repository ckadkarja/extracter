package com.jpa.extract.common.constant;

public class ApiConstant {
	public final static String PARAM_DATA   = "DATA";
	public final static String URL 			= "URL";
	public final static String MEDIA_TYPE	= "MediaType";
    public final static String HTTP_METHOD 	= "HttpMethod";
    public final static String NAVER_CLI_KEY	= "clientKey";
    public final static String NAVER_SEV_KEY	= "serverKey";
}

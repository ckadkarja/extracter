package com.jpa.extract.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * =====================================================
 * @author | jaeyoung
 * @mkdate | 2019. 8. 25.
 * @file   | SearchVO.java
 * @desc   | 네이버 EP 데이터 검색 조건
 * =====================================================
 */
@Getter
@Setter
@ToString
public class SearchVO {

	private String searchItem;
	private int display;
	private int start;
	private String sort;
	
	// 레퍼런스 default 값
	public SearchVO() {
		this.searchItem = "";
		this.display = 10;
		this.start = 1;
		this.sort = "sim";
	}
}

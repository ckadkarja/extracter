package com.jpa.extract.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * =====================================================
 * @author | jaeyoung
 * @mkdate | 2019. 8. 25.
 * @file   | Product.java
 * @desc   | Naver EP ������ POJO >> items
 * =====================================================
 */
@NoArgsConstructor(access=AccessLevel.PROTECTED)
@Getter
@Entity
@Table(name = "product")
@ToString(exclude = "productMeta")
public class Product {

	@JsonIgnore
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "pm_no")
	private ProductMeta productMeta;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "product_id")
    private Long productId;
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "link")
    private String link;
	
	@Column(name = "image")
    private String image;
	
	@Column(name = "lprice")
    private Long lprice;
	
	@Column(name = "hprice")
    private Long hprice;
	
	@Column(name = "mall_name")
    private String mallName;
    
	@Column(name = "product_type")
    private int productType;
	
	@Builder
	public Product(ProductMeta productMeta,
			Long productId,
			String title,
			String link,
			String image,
			Long lprice,
			Long hprice,
			String mallName,
			int productType) {
		this.productMeta = productMeta;
		this.productId = productId;
		this.title = title;
		this.link = link;
		this.image = image;
		this.lprice = lprice;
		this.hprice = hprice;
		this.mallName = mallName;
		this.productType = productType;
	}

}

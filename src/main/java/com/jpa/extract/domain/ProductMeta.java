package com.jpa.extract.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * =====================================================
 * @author | jaeyoung
 * @mkdate | 2019. 8. 25.
 * @file   | ProductMeta.java
 * @desc   | Naver EP 데이터 POJO // rss, channel 무시
 * =====================================================
 */
@NoArgsConstructor(access=AccessLevel.PROTECTED)
@Getter
@Entity
@Table(name = "product_meta")
@ToString(exclude = "productList")
public class ProductMeta {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "pm_no")
    private int pmNo;
    
    @Column(length = 100, name = "last_build_date")
    private String lastBuildDate;
    
    @Column(name = "total")
    private int total;
    
    @Column(name = "start")
    private int start;
    
    @Column(name = "display")
    private int display;
    
 	@OneToMany(mappedBy = "productMeta", cascade = CascadeType.ALL, fetch=FetchType.LAZY, orphanRemoval = true)
    private List<Product> productList = new ArrayList<>();
 	
 	@Builder
 	public ProductMeta(String lastBuildDate, 
 			int total, 
 			int start, 
 			int display) {
 		this.lastBuildDate = lastBuildDate;
 		this.total = total;
 		this.start = start;
 		this.display = display;
 	}
}

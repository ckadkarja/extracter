package com.jpa.extract.config;

import java.nio.charset.Charset;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public class RestTemplateInstance {

	private RestTemplate restTemplate = makeRestTemplate();
	private RestTemplateInstance() {}
	
	private static class SingletonHolder{
		private static final RestTemplateInstance instance = new RestTemplateInstance();
	}

	public static RestTemplateInstance getInstance() {
		return SingletonHolder.instance;
	}

	public RestTemplate getRestTemplate() {
		return this.restTemplate;
	}
	
	// restful pool ����
	private RestTemplate makeRestTemplate() {
		HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
		httpRequestFactory.setReadTimeout(1000 * 50 );
		httpRequestFactory.setConnectTimeout(1000 * 50);
		
		HttpClient httpClient = HttpClientBuilder.create()
				.setMaxConnTotal(100)
				.setMaxConnPerRoute(10)
				.setRetryHandler(new DefaultHttpRequestRetryHandler(3, true))
				.build();
		
		httpRequestFactory.setHttpClient(httpClient);
		
		RestTemplate restTemplate = new RestTemplate(httpRequestFactory);
		setCharUtf8(restTemplate);

		return restTemplate;
	}
	
	// utf-8 Ȱ��
	private void setCharUtf8(RestTemplate restTemplate) {
		
		for (HttpMessageConverter<?> messageConverter:restTemplate.getMessageConverters()) {
			
			if(messageConverter instanceof AllEncompassingFormHttpMessageConverter) {
				((AllEncompassingFormHttpMessageConverter) messageConverter).setCharset(Charset.forName("UTF-8"));
				((AllEncompassingFormHttpMessageConverter) messageConverter).setMultipartCharset(Charset.forName("UTF-8"));
			}
		}
	}
}

package com.jpa.extract.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DefaultDatabaseConf {

	@Value("${db.username}")
	private String dbUserName;
	@Value("${db.userpassword}")
	private String dbUserPassword;
	@Value("${db.driverclass}")
	private String dbDriverClassName;
	@Value("${db.url}")
	private String dbUrl;
	
	/**
	 * DataSource �� ���
	 * @return DataSource ��ü
	 */
	@Bean(destroyMethod = "")
	public DataSource dataSource() {
		BasicDBCPConf dataSource = new BasicDBCPConf();
		
		dataSource.setUsername(dbUserName);
		dataSource.setPassword(dbUserPassword);
		dataSource.setDriverClassName(dbDriverClassName);
		dataSource.setUrl(dbUrl);
		
		return dataSource;
	}
}

package com.jpa.extract.config;

import org.apache.commons.dbcp2.BasicDataSource;

public class BasicDBCPConf extends BasicDataSource {

	public BasicDBCPConf() {
		super.setTestOnBorrow(true);
		super.setTestOnReturn(true);
		super.setTestWhileIdle(false);
		super.setTimeBetweenEvictionRunsMillis(180000);
		super.setNumTestsPerEvictionRun(1);
		super.setMinEvictableIdleTimeMillis(180000);
	}

}

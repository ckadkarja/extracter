package com.jpa.extract.shceduler;

public interface EpSchedulerTask {

	void setSchedulerLog(long scheLogCode, String scheInfCode, String archGrpCode, String event, String msg);
    void setSchedulerInfo(String scheInfCode, String cron, String option, String archGrpCode);
    void execute();
}
